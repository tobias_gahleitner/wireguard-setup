#WireGuard

##Setup WireGuard Server
- Installation of Wireguard on the RPi (https://www.wireguard.com/)
- Generate public and private keypairs for server and client (https://www.wireguard.com/quickstart/#key-generation)
- Create a config file in `/etc/wireguard/wg0.conf` like:
```
[Interface]
PrivateKey = <SERVER_PRIVATE_KEY>
Address = 10.66.<LAST_DIGITS_OF_KITNUMBER>.1/24
ListenPort = 51820
SaveConfig = true
```
- start the service: `sudo wg-quick up wg0`
- check if it is running: `sudo wg`
- enable the client to connect using `sudo wg set wg0 peer <public key client> allowed-ips 10.66.<LAST_DIGITS_OF_KITNUMBER>.2`

##Setup WireGuard Client
- Installation of Wireguard on the client (https://www.wireguard.com/)
- Generate config file like:
```
[Interface]
PrivateKey = <client private key>
Address = 10.66.<LAST_DIGITS_OF_KITNUMBER>.2/24

[Peer]
PublicKey = <server public key>
AllowedIPs = 10.66.<LAST_DIGITS_OF_KITNUMBER>.0/24
Endpoint = <server ip address of physical interface>:51820
```